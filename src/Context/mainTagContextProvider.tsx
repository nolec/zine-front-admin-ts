import React, {useState} from "react";
import {MainTagContext} from "./context";

const MainTagContextProvider : React.FunctionComponent = ({children}) => {

    const randomKey = (length : number) => {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    const [searchKeywords, setSearchKeyWords] = useState('');
    const [renderKey, setRenderKey] = useState(randomKey(1));


    const filterFunc = (item : any) => {
        const searchingValue = searchKeywords.trim();
        if (searchingValue === '') {
            return true;
        } else {
            return item.tagname.includes(searchingValue)
        }
    }


    const handleZineTag = (e : any) => {
        const {value} = e.target;
        setSearchKeyWords(value);
        setRenderKey(randomKey(1));
    }


    const MainTagProvider = {
        handleZineTag,
        searchKeywords,
        renderKey,
        filterFunc
    };


    return <MainTagContext.Provider value={MainTagProvider}>{children}</MainTagContext.Provider>
}

export default MainTagContextProvider;