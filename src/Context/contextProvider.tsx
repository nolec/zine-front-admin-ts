import React, {useState, useEffect} from "react";
import {Context} from "./context";
import ReactDom from 'react-dom';
import {HtmlParser} from "../Components/ZineAdminComponents/HtmlParser";
import axios from 'axios';
import {useLazyQuery, useMutation} from "@apollo/client";
import {
    UPDATE_ARTICLE,
    GET_DATA,
    POST_ARTICLE,
    GET_LIST_DATA,
    GET_CONTENTS_IMG,
    GET_RECTANGLE_IMG,
    GET_SQUARE_IMG
} from "../Graphql";

export interface IPost {
    postTitle?: string
    postSubTitle?: string
    isVisible?: boolean
    squareThumbNail?: string
    rectangleThumbNail?: string
    parsingResult?: string
    postPreview?: string
    hashTag?: []
    goodsId?: string
    container: any[]
    photographerName? : string
    editorName? : string
    videoPD? : string
    immobilize? : number
    youtubeLink? : any
}

const ContextProvider : React.FunctionComponent = ({children}) => {

    const [param, setParam] = useState<any>();
    const [state, setState] = useState<any>({
        postTitle: '',
        postSubTitle: '',
        isVisible: true,
        squareThumbNail: '',
        rectangleThumbNail: '',
        parsingResult: '',
        postPreview: 'none',
        hashTag: [],
        goodsId: '',
        container: [],
        photographerName : '',
        editorName : '',
        videoPD : '',
        immobilize : 0,
    })
    const [deletedImg , setDeletedImg] = useState<any[]>([]);



    const [getEventList] = useLazyQuery(GET_LIST_DATA , {
        variables : {skip : 0, limit : 10}
    })

    const [updateArticle, {loading: updateLoading}] = useMutation(UPDATE_ARTICLE, {
        update(cache, {data: {updateArticle}}) {
            try {
                const {article}: any = cache.readQuery({query: GET_DATA, variables: {id: param}});
                cache.writeQuery({
                    query: GET_DATA,
                    variables: {id: param},
                    data: {article: {...updateArticle}}
                })
            } catch (e) {
                console.log(e);
                console.log('not updating Store - Projects not loaded yet');
            } finally {
                alert('저장되었습니다')
            }
        },
    });

    const [postArticle , {loading : postLoading}] = useMutation(POST_ARTICLE , {
        onCompleted : () => {
            getEventList()
        }
    })




    const [getSquareThumbnailURL, {data : squareThumbnailData, loading : squareThumbnailLoading}] = useLazyQuery(GET_SQUARE_IMG, {
        onCompleted: data => {
            console.log(data , "SQURE")
            setDeletedImg((prevState : any) => [...prevState , state.squareThumbNail])
            return setState({
                ...state,
                "squareThumbNail": data.getImgURL[0]
            });
        }
    })

    const [getRectangleThumbnailURL , {data : RectangleThumbnailData , loading : RectangleThumbnailDataLoading}] = useLazyQuery(GET_RECTANGLE_IMG , {
        onCompleted : data => {
            console.log(data , "RECTANGLE")
            setDeletedImg((prevState : any) => [...prevState , state.rectangleThumbNail])
            return setState({
                ...state,
                "rectangleThumbNail": data.getImgURL[0]
            });
        }
    })



    const settingForParam = (id : number) => {
        setParam(id);
    }


    /** 제목 , 부제목 , 상품번호 Input 을 제어 할수 있는 함수 입니다 */
    const handleInputState = (e : any, stateName : string) => {
        const {value} = e.target;
        setState({
            ...state,
            [stateName]: value
        })
    };
    /** 제목 , 부제목, 상품번호 Input 을 제어 할수 있는 함수 입니다 */




    /** 이미지 리스트에 들어오는 Element의 CheckBox ON/OFF 하는 함수 입니다 */
    const toggleChange = (target : any, index : number) => {
        const newContainer = state.container.map((item: any, itemIndex: number) => {
            if (index !== itemIndex) {
                return
            }
            const res = !item[target].use;

            return {
                ...item,
                [target]: {...item[target],"use": res, }
            }
        })

        setState((prevState:any) => ({
            ...prevState,
            container : newContainer
        }))
    };
    /** 이미지 리스트에 들어오는 Element의 CheckBox ON/OFF 하는 함수 입니다 */



    /** 노출 , 미노출 정하는 함수 입니다 */
    const toggleVisible = () => {
        const {isVisible} = state;
        if (isVisible) {
            setState({
                ...state,
                isVisible : false
            })
        } else {
            setState({
                ...state,
                isVisible : true
            })
        }
    }
    /** 노출 , 미노출 정하는 함수 입니다 */


    /** 고정 , 미고정을 정하는 함수 입니다 */
    const toggleImmobilize = (stateImmobilize : any) => {
        setState({
            ...state,
            immobilize: stateImmobilize
        })
    }
    /** 고정 , 미고정을 정하는 함수 입니다 */



    /** Input Text를 제어 하는 함수 입니다 */
    const handleInput = (e : any, index : number, target : any, secTarget : any) => {
        const {value} = e.target;
        const {container} = state;
        state.container[index][target][secTarget] = value;
        setState({
            ...state,
            container
        })
    };
    /** Input Text를 제어 하는 함수 입니다 */


    useEffect(() => {
        console.log(deletedImg, "deletedImg")
    }, [deletedImg])

    useEffect(() => {
        console.log(state.container, "state Container")
    }, [state.container])

    /** Container안에 있는 배열을 삭제하는 함수입니답 */
    const deleteElement = (index: number) => {
        const {container} = state;
        const deletedImg = container.filter((item: any, itemIndex: number) => index === itemIndex).map((item: any) => {
            if (item.alt) {
                return item.alt
            }
            return null
        });
        setDeletedImg((prevState: any) => [...prevState, ...deletedImg])

        const newContainer = container.filter((item: any, itemIndex: number) => index !== itemIndex);
        setState((prevState: any) => ({
            ...prevState,
            container: newContainer
        }))
    };
    /** Container안에 있는 배열을 삭제하는 함수입니답 */



    /** 상품 Api를 가져와서 State에 집어넣는 함수 입니다*/
    const ManufactureGoods = async () => {
        const {goodsId, container} = state;
        if (goodsId === '') {
            alert('상품번호를 적어주세요')
            return
        }
        const GoodsData = await axios.get('https://admin.thesaracen.com/md/goods/list-api', {
            params: {
                search_keyword: goodsId
            }
        })

        if(!GoodsData.data.success){
            alert('올바른 상품ID를 적어주세요');
            return
        }


        const data = GoodsData.data.datalist[0];


        const Goods = data.goods.filter((goods : any) => goods.id === parseInt(goodsId));
        const {name: brandName} = data.goods_one.goods_template.brand

        let goodsTitle = ''
        if (data.title.includes("#NAME#")) {
            const replaceName = data.title.replace("#NAME#", Goods[0].name)
            goodsTitle = `[${brandName}] ${replaceName}`
        } else {
            goodsTitle = `[${brandName}] ${data.title}`
        }


        const {thumbnail} = Goods[0];

        const realThumbnail = `https://saracen.azureedge.net/img/template/goods/${goodsId.toString()}/${thumbnail}`
        const retailSale = data.goods_template_price_retail.sale_price;
        const market = data.goods_template_price_retail.market_price;

        container.push({
            'title': goodsTitle,
            'key': goodsId,
            'image': realThumbnail,
            'alt': goodsTitle,
            'float': 'left',
            'link': goodsId,
            'price': {
                retailSale,
                market
            },
        })
        setState({
            ...state,
            container
        })
    }
    /** 상품 Api를 가져와서 State에 집어넣는 함수 입니다*/


    /** 엔터누를시 상품 Api 가져오게 함*/
    const keyUpGetGoods = (e : any) => {
        if (e.keyCode === 13) {
            ManufactureGoods();
        }
    }
    /** 엔터누를시 상품 Api 가져오게 함*/


    /** 유튜브 Input을 제어하는 함수 입니다*/
    const getYoutube = (e : any) => {
        const {value} = e.target;

        const youtubeQueryRegex = /(?<==).+/gm;
        const ManufactureYoutube = value.match(youtubeQueryRegex);

        setState({
            ...state,
            youtubeLink: ManufactureYoutube
        })
    }
    /** 유튜브 Input을 제어하는 함수 입니다*/

    /** 유튜브에 엔터 쳤을경우 ManufactureYoutube 가져옴 */
    const keyUpGetYoutube = (e : any) => {
        if (e.keyCode === 13) {
            ManufactureYoutube();
        }
    }
    /** 유튜브에 엔터 쳤을경우 ManufactureYoutube 가져옴 */


    /** 유튜브 Input value를 가져와서 가공하는 함수 입니다*/
    const ManufactureYoutube = () => {
        const {youtubeLink} = state;

        if (youtubeLink === undefined || youtubeLink === null) {
            alert('유튜브 링크를 적어주세요')
            return
        }

        const youtube: any = {
            'title': 'youtube',
            'image': youtubeLink.join(),
            'width': '',
            'noParsingInputValue': ''
        }
        setState((prevState: any) => ({
            ...prevState,
            container: [...prevState.container, youtube]
        }))
    }
    /** 유튜브 Input을 가져와서 가공하는 함수 입니다*/




    /** 유튜브 가로 사이즈 핸들링 하는 함수 입니다*/
    const handleYoutubeSize = (e : any, index : number) => {
        let {value} = e.target;
        if (parseInt(value) > 1150){
            value =  "1150"
        }

        const {container} = state;

        container[index].noParsingInputValue = value;

        let youtubeWidth = value / 1150 * 100;

        container[index].width = youtubeWidth

        setState({
            ...state,
            container
        })
    };
    /** 유튜브 가로 사이즈 핸들링 하는 함수 입니다*/


    /** 태그를 핸들링 하는 함수 입니다*/
    const handleTag = (e: any) => {
        const {value} = e.target;
        if (value.length > 7) {
            alert("최대 6글자 까지 가능합니다");
            e.target.value = "";
            return
        }
        const {hashTag}: any = state;
        if (value.includes(" ")) {
            if (value.includes(" ") && value.length === 1) {
                e.target.value = ""
                return
            }
            const replaceTag = value.replace(' ', "");
            if (hashTag?.findIndex((el: any) => el === replaceTag) > -1) {
                alert('이미 있는 태그입니다');
                e.target.value = "";
                return
            }

            const hashTagContainer = [...hashTag, replaceTag]

            setState({
                ...state,
                hashTag: hashTagContainer
            })
            e.target.value = ""
        }
    }
    /** 태그를 핸들링 하는 함수 입니다*/


    /** 태그를 삭제하는 함수 입니다*/
    const deleteTag = (index: number) => {
        const {hashTag} = state;
        const newHashTag = hashTag.filter((item: any, itemIndex: number) => itemIndex !== index);
        setState({
            ...state,
            hashTag: newHashTag
        })
    }
    /** 태그를 삭제하는 함수 입니다*/


    /** 섬네일의 사이즈 검사 와 및 미리보기를 보여주는 함수 입니다*/
    const thumbnail_Upload = (e: any, target: any) => {
        e.preventDefault();
        let item = e.target.files[0];

        let reader: any = new FileReader();
        reader.onloadend = () => {
            const img = new Image();
            img.src = reader.result
            img.onload = () => {
                if (img.naturalWidth === 1500 && img.naturalHeight === 1500 && target === 'squareThumbNail') {
                    return getSquareThumbnailURL({
                        variables : {file : e.target.files}
                    })
                }
                if (img.naturalWidth === 1500 && img.naturalHeight === 960 && target === 'rectangleThumbNail') {
                    return getRectangleThumbnailURL({
                        variables : {file : e.target.files}
                    })
                }
                return alert("이미지 크기를 다시 확인해주세요 !");
            }
        }
        if (item) {
            reader.readAsDataURL(item)
        }
    };
    /** 섬네일의 사이즈 검사 와 및 미리보기를 보여주는 함수 입니다*/


    /** 게시물 미리보기 함수 입니다*/
    const postPreview = async () => {
        let result = await ReactDom.render(HtmlParser(state) as any, document.getElementById('previewSection'));
        let parentsHtml : any = document.getElementById('previewSection');
        let outer = result.outerHTML;

        parentsHtml.innerHTML = outer;

        setState({
            ...state,
            parsingResult: outer,
            postPreview: 'block'
        })
    };
    /** 게시물 미리보기 함수 입니다*/

    /** 게시물 미리보기 닫는 함수 입니다*/
    const closePreview = () => {
        const {postPreview} = state;
        if (postPreview === 'block') {
            setState({
                ...state,
                postPreview: 'none'
            })
        }
    };
    /** 게시물 미리보기 닫는 함수 입니다*/


    /** 새글일때 저장버튼의 함수는 이 함수 입니다*/
    const postClick = async (history : any,postArticle : any) => {
        const {postTitle, squareThumbNail, rectangleThumbNail, hashTag, isVisible, parsingResult} = state;

        if (isVisible === false){
            await postArticle({
                variables: {
                    newArticle: {
                        title: state.postTitle,
                        subtitle: state.postSubTitle,
                        squareThumbnail: state.squareThumbNail,
                        rectangleThumbnail: state.rectangleThumbNail,
                        tags: state.hashTag,
                        isVisible: state.isVisible,
                        photographerName : state.photographerName,
                        editorName : state.editorName,
                        videoPD : state.videoPD,
                        mainTop : state.immobilize
                    },
                    newContainer: state.container,
                    deletedImg
                }
            })
            return
        }


        if (postTitle === '' || squareThumbNail === '' || rectangleThumbNail === '' || hashTag?.length === 0 || state?.container?.length === 0 || parsingResult === "") {
            alert('제목, 태그, 정사각형섬네일, 직사각형섬네일, 게시물 미리보기를 확인해주세요');
            return
        }

        await postArticle({
            variables: {
                newArticle: {
                    title: state.postTitle,
                    subtitle: state.postSubTitle,
                    squareThumbnail: state.squareThumbNail,
                    rectangleThumbnail: state.rectangleThumbNail,
                    tags: state.hashTag,
                    isVisible: state.isVisible,
                    photographerName : state.photographerName,
                    editorName : state.editorName,
                    videoPD : state.videoPD,
                    mainTop : state.immobilize
                },
                newContainer: state.container,
                deletedImg
            }
        })
    }
    /** 새글일떄 저장버튼의 함수는 이 함수 입니다*/



    /** 글을 수정할때는 이 함수 입니다*/
    const UpdateClick = async () => {
        const {postTitle, squareThumbNail, rectangleThumbNail, hashTag, isVisible, container} = state;

        if (isVisible === false){
            await updateArticle({
                variables: {
                    id: param,
                    article: {
                        title: state.postTitle,
                        subtitle: state.postSubTitle,
                        squareThumbnail: state.squareThumbNail,
                        rectangleThumbnail: state.rectangleThumbNail,
                        tags: state.hashTag,
                        isVisible: state.isVisible,
                        photographerName : state.photographerName,
                        editorName : state.editorName,
                        videoPD : state.videoPD,
                        mainTop : state.immobilize
                    },
                    updateContainer : state.container,
                    deletedImg
                },
            })
            return
        }
        if (postTitle === '' || squareThumbNail === '' || rectangleThumbNail === '' || hashTag?.length === 0 || container?.length === 0) {
            alert('제목, 태그, 정사각형섬네일, 직사각형섬네일, 게시물 미리보기를 확인해주세요');
            return
        }
        await updateArticle({
            variables: {
                id: param,
                article: {
                    title: state.postTitle,
                    subtitle: state.postSubTitle,
                    squareThumbnail: state.squareThumbNail,
                    rectangleThumbnail: state.rectangleThumbNail,
                    tags: state.hashTag,
                    isVisible: state.isVisible,
                    photographerName : state.photographerName,
                    editorName : state.editorName,
                    videoPD : state.videoPD,
                    mainTop : state.immobilize
                },
                updateContainer : state.container,
                deletedImg
            },
        })
    }
    /** 글을 수정할때는 이 함수 입니다*/




    /**===================================================여부터 ZineAdmin List 입니다===================================================*/

    const [listState, listsSetState] = useState({
        data: [],
        searchKeyWords: '',
        searchResult: window.location.href.includes('/tag/') ? decodeURIComponent(window.location.href.match(/(?<=\/tag\/).+/gm)?.join() as any) : null,
        pageActiveState: window.location.href.includes('/page/') ? parseInt(window.location.href.match(/(?<=\/)[0-9.]+/gm) as any) : 1,
        pageTotalCount: '',
        searchOption: 'tag'
    });
    const [searchParam, setSearchParam] = useState();




    /** Input에 검색단어를 State에 반환하는 함수 입니다*/
    const getSearchWord = (e : any) => {
        const {value} = e.target;
        listsSetState({
            ...listState,
            searchKeyWords: value.trim()
        })
    }
    /** Input에 검색단어를 State에 반환하는 함수 입니다*/


    /** Enter 눌렀을때 검색하는 함수 입니다*/
    const handleKeyUp = (e : any, history : any) => {
        const {searchKeyWords} = listState;

        if (e.keyCode === 8 && searchKeyWords.length === 0) {
            listsSetState({
                ...listState,
                searchResult: null,
                pageActiveState: 1,
            })
            history.push(`/zine-list/page/1`)
        }
        if (e.keyCode === 13) {
            searchingPost(history, searchParam)
        }
    }
    /** Enter 눌렀을때 검색하는 함수 입니다*/




    /** State에 값을 받아서 검색하고자 하는 값을 서칭해줍니다*/
    const searchingPost = (history : any, searchParam : any) => {
        const {searchKeyWords, searchOption} = listState;

        const blankPattern = /^\s+|\s+$/g;
        if (searchKeyWords.replace(blankPattern, "") === "") {
            alert('검색어를 넣어 주세요');
            return
        }

        listsSetState({
            ...listState,
            pageActiveState: 1,
            searchResult: searchKeyWords
        })
        history.push(`/zine-list/page/1/${searchOption}/${searchKeyWords}`)
    }
    /** State에 값을 받아서 검색하고자 하는 값을 서칭해줍니다*/




    /** Pagination 인덱스 값 조절하고 QueryString으로 반환하는 함수 입니다*/
    const handlePageChange = (pageNumber : any, history : any) => {
        const {searchResult, searchOption} = listState;

        listsSetState({
            ...listState,
            pageActiveState: pageNumber,
        })


        if (searchResult === null || searchResult === undefined) {
            history.push(`/zine-list/page/${pageNumber}`)
        } else {
            history.push(`/zine-list/page/${pageNumber}/${searchOption}/${searchResult}`)
        }
    }
    /** Pagination 인덱스 값 조절하고 QueryString으로 반환하는 함수 입니다*/


    /** Match를 가져와서 State에 반환하는 함수 입니다*/
    const setMatchParam = (match : any) => {
        setSearchParam(match)
    }
    /** Match를 가져와서 State에 반환하는 함수 입니다*/


    /**===================================================여기까지 ZineAdmin List 함수 입니다===================================================*/

    const provider = {
        state,
        param,
        setState,
        toggleVisible,
        handleInputState,
        settingForParam,
        handleTag,
        deleteTag,
        thumbnail_Upload,
        postClick,
        postPreview,
        toggleChange,
        deleteElement,
        handleInput,
        ManufactureYoutube,
        getYoutube,
        handleYoutubeSize,
        ManufactureGoods,
        closePreview,
        UpdateClick,
        keyUpGetYoutube,
        keyUpGetGoods,
        toggleImmobilize,
        /**===================================================이곳까지 ZineAdminList Export 입니다===================================================*/
        listState,
        handleKeyUp,
        searchingPost,
        getSearchWord,
        handlePageChange,
        setMatchParam,
        updateLoading,

        /**===================================================여기까지 ZineAdminList Export 입니다===================================================*/
    };



    /**==================뒤로,앞으로 가기 했을 경우 스테이트 변경=======================*/
    useEffect(() => {
        const currentURL = window.location.href;
        if (currentURL.includes("zine-list/page/")){
            const matching = window.location.href.match(/(?<=\/tag\/).+/gm) as any
            const {pageActiveState,searchResult} = listState;
            const currentState = parseInt(currentURL.match(/(?<=\/)[0-9.]+/gm) as any);
            const currentTag =  currentURL.includes('/tag/') ? decodeURIComponent(matching.join())  : null;
            if (currentState !== pageActiveState || currentTag !== searchResult){
                listsSetState({
                    ...listState,
                    pageActiveState: currentState,
                    searchResult : currentTag
                })
            }
        }
    },[window.location.pathname])
    /**==================뒤로,앞으로 가기 했을 경우 스테이트 변경=======================*/



    /**==================PageNumber 가 Num일 경우 Redirect page/1 로 이동=======================*/
    useEffect(() => {
        const {pageActiveState} = listState;
        if (isNaN(pageActiveState)){
            window.location.href = "/zine-list/page/1"
        }
    },[listState.pageActiveState])
    /**==================PageNumber 가 Num일 경우 Redirect page/1 로 이동=======================*/

    useEffect(() => {
        setState({
            ...state,
            postPreview: 'none'
        })
        setDeletedImg(() => []);
    },[window.location.href])

    /**==================container의 개수가 달라질때마다 parsingResut ''가 됌 , 미리보기 다시 클릭해줘야됌=======================*/
    useEffect(() => {
        setState({
            ...state,
            parsingResult: ''
        })
    },[state.container?.length])
    /**==================container의 개수가 달라질때마다 parsingResut ''가 됌 , 미리보기 다시 클릭해줘야됌=======================*/



    useEffect(() => {

        if (param === "new") {
            setState({
                postTitle: '',
                postSubTitle: '',
                isVisible: true,
                squareThumbNail: '',
                rectangleThumbNail: '',
                parsingResult: '',
                postPreview: 'none',
                hashTag: [],
                goodsId: '',
                photographerName: '',
                editorName: '',
                videoPD: '',
                container: [],
                immobilize : 0,
            })
        }
    },[param])

    return <Context.Provider value={provider}>{children}</Context.Provider>
}

export default ContextProvider;
