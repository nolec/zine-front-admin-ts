import React, {useContext} from 'react'
import {Context} from "../../Context/context";
import SS from '@saraceninc/saracen-style-ts'
import {Link} from 'react-router-dom'

interface IProps {
    item : any
    index : number
}

const EachPost : React.FunctionComponent<IProps> = ({item,index}) => {
    const {settingForParam} = useContext(Context);

    return (
        <Link key={index} to={`/zine-list/${item.id }`} onClick={() => settingForParam(item.id)}>
            <SS.Core.RowF minWidth='1150px' maxWidth='1500px' flexDirection='row' alignItems='center' style={{width :'100%',margin : '0 0 15px 0','borderBottom': '1px solid #e2e2e2' ,'padding':'0 0 10px 0'}}>
                <SS.Core.Span style={{'flex': '1', 'textAlign': 'center','textIndent':'22px'}}>{item.tags[0]}</SS.Core.Span>
                <SS.Core.Span style={{'flex': '3', 'textAlign': 'center'}}>{item.title}</SS.Core.Span>
                <SS.Core.Span style={{'flex': '1', 'textAlign': 'center'}}>{item.createdAt.substring(0, 10)}</SS.Core.Span>
            </SS.Core.RowF>
        </Link>
    )
}


export default EachPost