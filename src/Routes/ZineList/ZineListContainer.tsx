import React, { useState } from 'react';
import ZineListPresenter from './ZineListPresenter';

interface IState {
    currentPage : number
}

const ZineListContainer : React.FunctionComponent = () => {

    const [state, setState] = useState<IState>();

  const handleChange = (e : any,index : number) => {
    setState({
      currentPage : index
    })
  }


  const handleKeyUp = (e : any) => {
    if (e.keyCode === 13){
  
    }
  }

  const searchingPost = () => {

  }



    return (
        <>
          <ZineListPresenter
              currentPage = {state?.currentPage}
              handleKeyUp = {handleKeyUp}
              handleChange = {handleChange}
              searchingPost = {searchingPost}
          />
        </>
    );
}
export default ZineListContainer;