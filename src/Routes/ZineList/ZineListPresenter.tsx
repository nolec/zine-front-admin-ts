import React, {useContext, useEffect} from 'react'
import { Link , withRouter} from 'react-router-dom'
import SS from '@saraceninc/saracen-style-ts'
import {Context} from "../../Context/context";
import EachPost from './EachPost'
import Pagination from "react-js-pagination";
import "bootstrap/dist/css/bootstrap.min.css";
import { GET_DATA, GET_TOTAL_COUNT } from 'src/Graphql';
import { useQuery } from '@apollo/client';
import styled from "styled-components";

const CustomRow = styled(SS.Core.Row)`
min-width : 1150px;
max-width : 1500px;
display : flex;
margin : 0 auto;
 width : 100% ;
  padding : 0 15px;
`;

const ZineListPresenter : any = withRouter(({history,match}) => {

        const {listState,handlePageChange,handleKeyUp,searchingPost,getSearchWord,setMatchParam,settingForParam} = useContext(Context);
        const {pageActiveState , searchResult , searchKeyWords} = listState

        useEffect(() => {
            setMatchParam(match)
        },[match])



        const {loading : countLoading, data : totalCount} = useQuery(GET_TOTAL_COUNT,{
            variables : {tag : searchResult}
        });

        const {loading : dataLoading, data : dataList} = useQuery(GET_DATA, {
            variables : {skip: (10 * (pageActiveState - 1)) , limit: 10 , tag: searchResult}
        });
        

        if (dataLoading || countLoading) {
            return (
                <SS.Loader/>
            )
        }



    return (
        <>
            <SS.Core.Row >
                <CustomRow>
                    <SS.Core.Span fontSize='1.5em' fontWeight='600' style={{'flex': '1', 'textAlign': 'center'}}>카테고리</SS.Core.Span>
                    <SS.Core.Span fontSize='1.5em' fontWeight='600' style={{'flex': '3', 'textAlign': 'center'}}>컨텐츠 이름</SS.Core.Span>
                    <SS.Core.Span fontSize='1.5em' fontWeight='600' style={{'flex': '1', 'textAlign': 'center'}}>생성 일자</SS.Core.Span>
                </CustomRow>
                <SS.Core.RowF flexDirection='column' >
                    {dataList?.articles.map((item : any, index : number) => (
                        <EachPost
                            item = {item}
                            key = {index}
                            index = {index}
                        />
                    ))}
                </SS.Core.RowF>
                <SS.Core.RowF flexDirection='row' alignItems={'center'} justifyContent={'center'} style={{ margin : '20px 0 0 0'}}>
                    <Pagination
                        itemClass="page-item"
                        linkClass="page-link"
                        activePage={pageActiveState}
                        itemsCountPerPage={10}
                        totalItemsCount={totalCount?.totalCount}
                        pageRangeDisplayed={10}
                        onChange={(pageNumber : number) => handlePageChange(pageNumber,history)}
                    />
                </SS.Core.RowF>
                <SS.Core.Row style={{alignItems:'center'}}>
                    <Link to ='/zine-list/new'>
                        <SS.Core.Button onClick={() => settingForParam('new')} className='green' width='70px'fontSize='17px' style={{'float':'right'}}>추가</SS.Core.Button>
                    </Link>
                </SS.Core.Row>
                <SS.Core.RowF justifyContent='center' alignItems='center'>
                    <SS.Core.Input type="search" placeholder='태그로 검색' style={{'width':'25%','height':'30px','marginLeft':'50px'}} onChange={getSearchWord} value={searchKeyWords} onKeyUp={(e : any) => handleKeyUp(e,history)}/>
                    <SS.Core.Button className={'blue'} width='60px'  style={{height:'30px','textAlign':'center'}} onClick={() => searchingPost(history)}>검색</SS.Core.Button>
                </SS.Core.RowF>
            </SS.Core.Row>
        </>
    )
})


export default ZineListPresenter
