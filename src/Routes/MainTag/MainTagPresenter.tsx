import React, {useContext} from 'react';
import SS from '@saraceninc/saracen-style-ts'
import styled from 'styled-components'
import {MainTagContext} from "../../Context/context";
import {useMutation, useQuery} from "@apollo/client"
import {POST_ISMAIN, GET_ZINE_TAG_ALL} from "../../Graphql"

interface IStyle {
    floatnone? : string
    padding? : string
    textAlign? : string
    fontSize? : string
}

const SearchGroup = styled.div`
  display: flex;
  flex: 1;
  margin-bottom: 10px;
`;

const SearchBox = styled.input<IStyle>`
    border: solid 1px silver;
    float: ${props => props.floatnone ? "none" : "left"};
    padding: ${props => props.padding ?? '12px 10px'};
    height: auto;
    text-align: ${props => props.textAlign};
    position: relative;
    font-size: ${props => props.fontSize ?? '20px'};
    border-radius: 5px;
    margin-right: 0.3%;
    &[type='text']{
    flex : 3;
    }
    &[type='search']{
    flex: 7;
    }
    @media only screen and (max-width: 600px) {
    width: 100%;
    }
`;

const CategoryWrap = styled.div`
    display : flex;
    width : 100%;
    margin-top : 20px;
    justify-content : space-between;
    align-items : center;
`

const CategorySpan = styled.span`
    display : block;
    float : left;
    font-size : 1.5rem;
    text-align: center;
    margin-bottom : 20px;
`

const TagWrap = styled.div`
    display : flex;
    width : 100%;
    margin-top : 20px;
    justify-content : space-between;
    align-items : center;
    &:hover{
        background-color : rgba(244,245,245,0.7);
        border-radius : 15px;
    }
`

const Tag = styled.span`
    display : block;
    float : left;
    font-size : 1.2rem;
    text-align: center;
    padding-left : 7px;
`
const MainTagPresenter:React.FunctionComponent = () => {

    const {handleZineTag, filterFunc,renderKey} = useContext(MainTagContext);



    /**************** Mutation Query ****************/
    const [postIsMain, {loading: isMainLoading}] = useMutation(POST_ISMAIN);
    /**************** Mutation Query ****************/


    /**************** UseQuery 데이터 ****************/
    const {data: allZineTags, loading: zineTagLoading} = useQuery(GET_ZINE_TAG_ALL);
    /**************** UseQuery 데이터 ****************/

    const handleMain = async (item : any) => {
        let result = allZineTags?.getTagsAll?.filter((tags : any) => tags?.id === item?.id);
        await postIsMain({
            variables: {
                tagId: result[0].id,
                currentBool: result[0].isMain
            }
        })
    }



    if (zineTagLoading) {
        return (
            <SS.Loader/>
        )
    }



    return (
        <>
            <SS.Core.Container style={{'backgroundColor': 'transparent', 'maxWidth': '1150px'}}>
                <SearchGroup>
                    <SearchBox type="search" placeholder="태그 검색" onChange={(e) => handleZineTag(e)}/>
                </SearchGroup>
                <SS.Core.Container padding='30px 200px'>
                    <CategoryWrap>
                        <CategorySpan>태그 이름</CategorySpan>
                        <CategorySpan>메인 여부</CategorySpan>
                    </CategoryWrap>
                    {allZineTags.getTagsAll?.filter((item : any) => filterFunc(item)).map((item:any, index : number) => {
                        return (
                            <TagWrap key={renderKey + index}>
                                <Tag>{item?.tagname}</Tag>
                                <SS.Toggle checked={item.isMain} onChange={() => handleMain(item)} margin='0' top='-2px'  width='31px' height='100%'/>
                            </TagWrap>
                        )
                    })}
                </SS.Core.Container>
            </SS.Core.Container>
        </>
    )
}

export default MainTagPresenter