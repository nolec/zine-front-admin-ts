import React, {useContext} from 'react'
import SS from '@saraceninc/saracen-style-ts'
import styled from 'styled-components'
import DistinguishData from '../../Components/ZineAdminComponents/DistinguishData';
import {Textarea} from "../../Components/ZineAdminComponents/ByOptionDynamic/ImageDynamicOptionBar";
import {withRouter} from "react-router-dom";
import {Context} from "../../Context/context";
import PreviewPost from "../../Components/ZineAdminComponents/PreviewPost";
import {GET_DATA,  POST_ARTICLE} from "../../Graphql";
import { useMutation } from '@apollo/client';
import ReactDropZone from "../../Components/ZineAdminComponents/reactDropzone/dropZone";

interface IStyle {
    bgColor : string
    width : string
}

const InputText = styled.input.attrs((props : any) => ({
    type: props.type ?? 'text',
    placeholder: props.placeholder,
    maxlength: props.maxLength,
}))`
    background-color: white;
    font-size : 14px;
    border-radius : 3px;
    flex : ${(props : any) => props.flex ?? 1};
    width : ${(props : any) => props.width};
    height : ${(props : any) => props.height};
    align-items: normal;
    border : none;
`


const VisibleButton = styled.button<IStyle>`
  padding: ${(props : any) => props.padding};
  font-size: ${(props : any) => props.fontSize ?? '14px'};
  font-weight: 400;
  border-radius: ${(props : any) => props.round ? '5em' : '.3rem'};
  letter-spacing: -1px;
  border: 0;
  cursor: pointer;
  width: ${(props : any) => props.width ?? '100%'};
  height: ${(props : any) => props.height ?? '100%'};
  margin : ${(props : any) => props.margin};
  padding : 4px 0;
  background-color : ${(props : any) => props.bgColor};
  &.isVisible{
    border : solid 1px #e981a6;
    color : #e981a6;
  }
`


const ZineNewAdminPresenter = withRouter(({history}) => {

    //함수
    const {
        state, toggleVisible, handleInputState, handleTag, deleteTag, thumbnail_Upload, postClick, postPreview,
        fileUploadPreview, toggleChange, deleteElement, handleInput, ManufactureYoutube, getYoutube, handleYoutubeSize, ManufactureGoods, keyUpGetYoutube, keyUpGetGoods,
        updateLoading, toggleImmobilize
    } = useContext(Context);



    const {
        isVisible, hashTag, postSubTitle, squareThumbNail, rectangleThumbNail, imageWidth, noLink, noTextArea, noImgTitle, imgTitle, imgColorValue, imgExplanation, divLink, file, container, immobilize,
    } = state

    const [postArticle,{loading:postLoading}] = useMutation (POST_ARTICLE, {
        update(cache, {data:{postArticle} }) {
            try {
                const {articles} : any = cache.readQuery({query: GET_DATA, variables : {skip:0,limit:10,tag:null}});
                cache.writeQuery({
                    query : GET_DATA,
                    variables: {skip: 0, limit: 10, tag: null},
                    data : {
                        articles : [postArticle , ...articles]
                    }
                })
            } catch (e) {
                return undefined;
            }
        },


        onCompleted : ({articles}) => {
            alert('저장 됐습니다');

            history.push('/zine-list/page/1')
    }});






    return (
        <>
            <PreviewPost/>
            {postLoading ? <SS.Loader/> : ''}
            {updateLoading ? <SS.Loader/> : ''}
            <SS.Core.Container style={{'backgroundColor': 'transparent', 'maxWidth': '1150px'}}>

            <SS.Core.RowF flexDirection="row" alignItems='center' padding='0 0 10px 0'>
                    <SS.Core.Span fontSize='18px' margin='0 29px 0 0' style={{'width':'70px'}}>상단 고정</SS.Core.Span>
                    <VisibleButton bgColor='white' className={immobilize === 0 ? 'isVisible' : ''} onClick={() => toggleImmobilize(0)} width='150px'>고정 안함</VisibleButton>
                    <VisibleButton bgColor='white' className={immobilize === 1 ? 'isVisible' : ''} onClick={() => toggleImmobilize(1)} width='150px'>왼쪽 고정</VisibleButton>
                    <VisibleButton bgColor='white' className={immobilize === 2 ? 'isVisible' : ''} onClick={() => toggleImmobilize(2)} width='150px'>오른쪽 고정</VisibleButton>
                </SS.Core.RowF>

                <SS.Core.RowF flexDirection="row" alignItems='center'>
                    <SS.Core.Span fontSize='18px' margin='0 29px 0 0' style={{'width':'70px'}}>공개</SS.Core.Span>
                    <VisibleButton bgColor='white' className={isVisible ? 'isVisible' : ''} onClick={toggleVisible} width='150px'>노출</VisibleButton>
                    <VisibleButton bgColor='white' className={!isVisible ? 'isVisible' : ''} onClick={toggleVisible} width='150px'>비노출</VisibleButton>
                </SS.Core.RowF>


                <SS.Core.RowF flexDirection='row' style={{width:'100%',margin:'7px 0'}}>
                    <SS.Core.RowF flexDirection='row' justifyContent='center' alignItems='center' style={{minHeight : '24px'}}>
                        <SS.Core.Span fontSize='17px' margin='0 15px 0 0'>에디터</SS.Core.Span>
                        <InputText style={{flex : '0 0'}} height='27px' placeholder='이름을 적어주세요' onChange={(e) => handleInputState(e,'editorName')} value={state.editorName ? state.editorName : ""}/>
                        <SS.Core.Span fontSize='17px'  margin='0 14px'>포토그래퍼 이름</SS.Core.Span>
                        <InputText style={{flex : '0 0'}} height='27px' placeholder='이름을 적어주세요' onChange={(e) => handleInputState(e,'photographerName')} value={state.photographerName ? state.photographerName : ""}/>
                        <SS.Core.Span fontSize='17px'  margin='0 14px'>영상 PD</SS.Core.Span>
                        <InputText style={{flex : '0 0'}} height='27px' placeholder='이름을 적어주세요' onChange={(e) => handleInputState(e,'videoPD')} value={state.videoPD ? state.videoPD : ""}/>
                    </SS.Core.RowF>
                </SS.Core.RowF>


                <SS.Core.RowF flexDirection="row" alignItems='center'style={{margin : '0 0 15px 0'}} >
                    <SS.Core.Span fontSize='18px' margin='0 29px 0 0'>제목</SS.Core.Span>
                    <InputText placeholder="제목을 입력해주세요" height="27px" value={state.postTitle ? state.postTitle : ""} onChange={(e) => handleInputState(e, 'postTitle')}/>
                </SS.Core.RowF>


                <SS.Core.RowF flexDirection='row' alignItems='center' style={{minHeight : '0'}}>
                    <SS.Core.Span fontSize='18px' margin='0 29px 0 0'>태그</SS.Core.Span>
                    <InputText height='27px' placeholder='#빼고 단어를 적어주세요' onChange={(e) => handleTag(e)}/>
                </SS.Core.RowF>

                <SS.Core.Tags padding='0 0 0 60px' style={{display: 'flex','flexWrap': 'wrap',flexDirection : 'row',alignItems : 'left',minHeight : '0'}}>
                    {hashTag?.map((item : any, index : number) => (
                        <span className={'pink-outline'} key={index}  color={'#333'} style={{'margin':'13px 13px 0 0',padding:'1px','borderRadius' : '25px' }}>
                            {`#${item}`}
                            <span onClick={() => deleteTag(index)} style={{
                                'cursor': 'pointer',
                                'padding': '3px',
                                'margin' : '0 0 0 5px'
                            }}>X</span>
                        </span>
                    ))}
                </SS.Core.Tags>

                <SS.Core.RowF flexDirection="row" style={{margin : "15px 0"}}>
                    <SS.Core.Span fontSize='18px' margin='0 14px 0 0'>부제목</SS.Core.Span>
                    <Textarea placeholder='최대 50글자' maxLength={50} value={postSubTitle} style={{width : '986px', height : '80px' }} onChange={(e) => handleInputState(e, 'postSubTitle')}/>
                </SS.Core.RowF>


                <SS.Core.RowF flexDirection='row' style={{ minHeight : '0',margin : '10px 0 10px 0'}}>

                    <SS.Core.RowF flexDirection='row' style={{ minHeight : '0'}}>
                    <SS.Core.Row minHeight='0' style={{ flex : '0.85'}}>
                            <SS.Core.Span style={{'float': 'left', 'letterSpacing': '1px'}}>정사각형 섬네일</SS.Core.Span>
                            <SS.Core.Span style={{
                                'fontSize': '9.7px',
                                'color': '#c0392b',
                                'fontWeight': 600,
                                'margin': '4px 0 7px 0'
                            }}>
                                (1500px * 1500px)
                            </SS.Core.Span>
                            {/*파일 업로드 시작*/}
                            <SS.Core.Input type="file" id='file'
                                           onChange={(e) => thumbnail_Upload(e, "squareThumbNail")}
                                           accept="image/*"/>
                            <SS.Core.Label width='10%' className='file' htmlFor='file'>업로드</SS.Core.Label>
                            {/*파일 업로드 끝*/}
                        </SS.Core.Row>
                        <SS.Core.Row style={{flex:'2'}}>
                            <SS.Core.Row width='200px' height='200px'>
                                <img src={`${process.env.REACT_APP_TENCENT_IMG_URL}${squareThumbNail}`} style={{'width':'100%','height':'100%'}} alt={"정사각형 섬네일"}/>
                            </SS.Core.Row>
                        </SS.Core.Row>

                        {/*직사각형 섬네일*/}
                        <SS.Core.Row minHeight='0'style={{ flex : '0.85'}}>
                            <SS.Core.Span style={{'float': 'left', 'letterSpacing': '1px'}}>직사각형 섬네일</SS.Core.Span>
                            <SS.Core.Span style={{
                                'fontSize': '9.7px',
                                'color': '#c0392b',
                                'fontWeight': 600,
                                'margin': '4px 0 7px 0'
                            }}>
                                (1500px * 960px)
                            </SS.Core.Span>
                            {/*파일 업로드 시작*/}
                            <SS.Core.Input type="file" id='rectangleThumbNail'
                                           onChange={(e) => thumbnail_Upload(e, "rectangleThumbNail")}
                                           accept="image/*"/>
                            <SS.Core.Label width='10%' className='file' htmlFor='rectangleThumbNail'>업로드</SS.Core.Label>
                            {/*파일 업로드 끝*/}
                        </SS.Core.Row>
                        <SS.Core.Row style={{ flex : 2}}>
                            <SS.Core.Row width='200px' height='115px'>
                                <img src={`${process.env.REACT_APP_TENCENT_IMG_URL}${rectangleThumbNail}`} style={{'width':'100%','height':'100%'}} alt={"직사각형 섬네일"}/>
                            </SS.Core.Row>
                        </SS.Core.Row>
                    </SS.Core.RowF>
                </SS.Core.RowF>


                <SS.Core.Span>파일 업로드</SS.Core.Span>
                <SS.Core.RowF bgColor='#e2e2e2' alignItems='center' justifyContent='center' style={{margin : '15px 0',height : '150px'  }}>
                    <ReactDropZone/>
                </SS.Core.RowF>

                <SS.Core.Row minHeight='0'>
                    <SS.Core.Span lineHeight='31px'>이미지 리스트</SS.Core.Span>


                    {/*유튜브 링크 가져오기*/}

                    <SS.Core.Button className={'red'} onClick={ManufactureYoutube} width='auto' style={{
                        'lineHeight': '25px',
                        'float': 'right',
                        'color': 'white',
                        height : 'auto' 
                    }}>유튜브 추가</SS.Core.Button>
                    <InputText onKeyUp={(e) => keyUpGetYoutube(e)} onChange={(e) => getYoutube(e)} placeholder='유튜브 링크를 기입해주세요' style={{'float': 'right'}} width='400px' height='28px'/>
                    {/*유튜브 끝*/}

                    {/*상품 검색 시작*/}
                    <SS.Core.Button onClick={ManufactureGoods} width='auto' style={{
                        'backgroundColor': '#e981a6',
                        'lineHeight': '25px',
                        'float': 'right',
                        'color': 'white',
                        'margin': '0 30px 0 0',
                        height : 'auto' 
                    }}>상품 추가</SS.Core.Button>
                    <InputText type="number" onKeyUp={(e) => keyUpGetGoods(e)} onChange={(e) => handleInputState(e,'goodsId')} placeholder='상품 번호를 기입해주세요' maxLength={6} style={{'float': 'right'}} width='250px' height='28px'/>
                </SS.Core.Row>
                {/*유튜브 링크 가져오기*/}

                <SS.Core.Row id='imageList' bgColor='#e2e2e2' margin='15px 0' height='750px' padding='10px' overflow='auto'>
                    {container?.map((item : any, index : number) => (
                        <DistinguishData
                            item={item}
                            index={index}
                            imageWidth={imageWidth}
                            key={index}
                            noLink={noLink}
                            toggleChange={toggleChange}
                            deleteElement={deleteElement}
                            handleInput={handleInput}
                            noTextArea={noTextArea}
                            noImgTitle={noImgTitle}
                            imgTitle={imgTitle}
                            imgColorValue={imgColorValue}
                            imgExplanation={imgExplanation}
                            divLink={divLink}
                            handleYoutubeSize={handleYoutubeSize}
                            file={file}
                        />
                    ))}
                </SS.Core.Row>
                <SS.Core.RowF flexDirection='row' alignItems='center' justifyContent='space-between' style={{minHeight:'0',height:'30px'}}>
                    <SS.Core.Button className={'blue'} width='49%' onClick={postPreview}> 게시물 미리보기</SS.Core.Button>
                    <SS.Core.Button className={'green'} width='49%' onClick={() => postClick(history,postArticle)}>게시물 저장하기</SS.Core.Button>
                </SS.Core.RowF>
            </SS.Core.Container>
        </>
    )
})

export default ZineNewAdminPresenter