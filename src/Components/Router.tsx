import React from 'react'
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect,
    Link
} from "react-router-dom";
import ZineList from "../Routes/ZineList";
import ZineAdmin from "../Routes/ZineAdmin";
import MainTagPresenter from "../Routes/MainTag/MainTagPresenter";

import ZineNewAdminPresenter from "../Routes/ZineAdmin/ZineNewAdminPresenter";
import SS from "@saraceninc/saracen-style-ts";
import styled from "styled-components";

interface IStyle {
    current? : string
}

const Aside = styled.div`
top: 0;
width: 13%;
position: fixed;
height: 100%;
background: #fff;
z-index: 999;
will-change: transform;
transition: transform .2s ease-in-out, -webkit-transform .2s ease-in-out;
box-shadow: 0 2px 10px rgba(90,97,105,.1), 0 2px 5px rgba(90,97,105,.12), 0 1px 2px rgba(90,97,105,.1), 0 0.5px 2px rgba(165,182,201,.1);
box-sizing: border-box;
`

const A = styled(Link)<IStyle>`
display: flex;
align-items: center;
justify-content: left;
padding: 15px 25px;
color: 
${props => (props.current ? "#fd79a8" : "#57606f")};
background-color: 
${props => (props.current ? "#ecf0f1" : "transparent")};
border-radius:
${props => (props.current ? "20px" : "0")};
position: relative;
&:hover{
text-decoration : none;
color: #fd79a8;
background-color: #ecf0f1;
border-radius: 20px;
transition: color 0.2s ease-in-out, background-color 0.2s ease-in-out;
}
&.logo{
padding: 0px;
&:hover{
  color: #57606f;
  background-color: white;
  transition: color 0.2s ease-in-out;
  
}
&>span{
display: block;
width: 100%;
font-weight: 600;
font-size: 2em;
color: #fd79a8;
text-align: center;
padding: 20px 0px;
}
}
&>i{
font-size: 1.4em;
padding-right: 5px;
}
&.dropdown-toggle:after{
display: inline-block;
width: 0;
height: 0;
margin-left: .255em;
vertical-align: .255em;
content: "";
border-top: .3em solid;
border-right: .3em solid transparent;
border-bottom: 0;
border-left: .3em solid transparent;
position: absolute;
right: 30px;
}
@media only screen and (max-width: 992px) {
& span.m{
  display: block;
}
& span.pc{
  display: none;
}
justify-content: center;
& span{
  display: none;
}
&.dropdown-toggle:after{
  display: none;
}
}
@media only screen and (min-width: 992px) {
& span.m{
  display: none;
}
}  
`

export const RouterComponent :React.FunctionComponent = () => {

    return(
        <Router>
            <>
                <Aside>
                    <A to="/zine-list/page/1" className="logo">
                        <span className="pc" role="img" aria-label="logo">💅🏼Zine관리자</span>
                        <span className="m" role="img" aria-label="logo">💅🏼</span>
                    </A>

                    <A to='/zine-list/page/1'>
                        <span className="pc" role="img" aria-label="logo">게시물 관리</span>
                    </A>
                    <A to='/zine-list/maintag'>
                        <span className="pc" role="img" aria-label="logo">메인태그 관리</span>
                    </A>

                </Aside>
                <SS.Header navs={[]}/>
                <SS.Core.Main justifyContent="center" flexDirection="column">
                    <Switch>
                        <Route path='/zine-list/maintag/:searchKeywords?' exact component={MainTagPresenter}/>
                        <Route path="/zine-list/page/:pageActiveState/:searchOption?/:searchKeyWords?" exact component={ZineList}/>
                        <Route path="/zine-list/new" exact component={ZineNewAdminPresenter}/>
                        <Route path="/zine-list/:id" exact component={ZineAdmin}/>
                        <Redirect from="*" to="/zine-list/page/1"/>
                    </Switch>
                </SS.Core.Main>
            </>
        </Router>
    );
}