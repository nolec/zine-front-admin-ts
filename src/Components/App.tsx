import React from "react";
import {RouterComponent} from "../Components/Router";
import SS from "@saraceninc/saracen-style-ts";
import ContextProvider from "../Context/contextProvider";
import MainTagContextProvider from "../Context/mainTagContextProvider";
import { ApolloProvider } from "@apollo/client";
import client from "src/Graphql/client";
import theme from "@saraceninc/saracen-style-ts/lib/theme";
import {ThemeProvider} from "@saraceninc/saracen-style-ts/lib/typed-components";

const App : React.FunctionComponent  = () => {
        return (
            <ApolloProvider client={client}>
                <ThemeProvider theme={theme}>
                    <ContextProvider>
                        <MainTagContextProvider>
                            <RouterComponent/>
                            <SS.GlobalStyles/>
                        </MainTagContextProvider>
                    </ContextProvider>
                </ThemeProvider>
            </ApolloProvider>
        );
}

export default App;