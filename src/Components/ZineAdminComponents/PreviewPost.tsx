import React, {useContext} from 'react';
import SS from "@saraceninc/saracen-style-ts";
import {Context} from "../../Context/context";

const PreviewPost = () => {

    const {state, closePreview} = useContext(Context)
    const {postPreview} = state;

    return (
        <SS.Core.Row  width='100%' height='100%' margin='0 auto' style={{
            position: 'fixed',
            top: '50%',
            left: '50%',
            zIndex : 10000,
            transform: 'translate(-50%,-50%)',
            backgroundColor: 'rgba(0,0,0,0.7)',
            display: `${postPreview}`,
            overflow: 'auto'
        }}>
            <SS.Core.Span style={{'cursor': 'pointer', 'position': 'sticky', 'top': '0', 'left': '0', 'right': '0','bottom':'0'}}
                          fontSize='37px' color='white' fontWeight='600' onClick={closePreview}>X</SS.Core.Span>
            <SS.Core.Row id='previewSection' width='1150px' height='auto' margin='0 auto' textAlign='left'
                         bgColor='white' overflow='auto' minHeight='0'>

            </SS.Core.Row>
        </SS.Core.Row>

    )
}

export default PreviewPost
