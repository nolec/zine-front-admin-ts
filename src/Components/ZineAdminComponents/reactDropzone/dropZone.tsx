import React, {useContext} from 'react';
import {useDropzone} from 'react-dropzone';
import {useLazyQuery} from '@apollo/client';
import {GET_CONTENTS_IMG} from '../../../Graphql';
import {Context} from "../../../Context/context";
import SS from '@saraceninc/saracen-style-ts'


const ReactDropZone = (props: any) => {
    const {setState} = useContext(Context);

    const [uploadImg, {loading: uploadLoading}] = useLazyQuery(GET_CONTENTS_IMG, {
        onCompleted: async (data) => {
            await data.getImgURL.forEach((item: string, index: number) => {
                const img = new Image();
                img.onload = () => {
                    if (img.naturalWidth > 1150) {
                        return alert("1150px 이하의 이미지를 넣어주세요 !");
                    }
                    const imageWidth = Math.floor((img.width / 1150 * 100) * 100) / 100;
                    const float = imageWidth === 100 ? 'none' : 'left';
                    const obj = {
                        'key': String(index),
                        'image': `${process.env.REACT_APP_TENCENT_IMG_URL}${item}`,
                        'width': imageWidth,
                        'alt': item,
                        'float': float,
                        'imgLink': {
                            'use': false,
                            'href': ''
                        },
                        'imgTitle': {
                            'use': false,
                            'value': '',
                            'color': ''
                        },
                        'imgExplanation': {
                            'use': false,
                            'value': ''
                        },
                    }

                    setState((prevState: any) => ({
                        ...prevState,
                        container: [...prevState.container, obj]
                    }))
                }
                img.src = `${process.env.REACT_APP_TENCENT_IMG_URL}${item}`
            })
        }
    })


    const {getRootProps, getInputProps} = useDropzone({
        accept: 'image/*',
        onDrop: (acceptedFiles) => {

            const preview = acceptedFiles.map((file) =>
                Object.assign(file, {
                    preview: URL.createObjectURL(file),
                })
            );
            console.log(preview)
            preview.forEach((item: any) => {
                const img = new Image();
                img.onload = () => {
                    if (img.naturalWidth > 950) {
                        alert('가로 크기 950이하의 이미지를 업로드 해주세요');
                        return;
                    } else {
                        uploadImg({
                            variables: {file: preview}
                        });
                    }
                };
                img.src = item.preview;
            });
        },
    });


    if (uploadLoading) {
        return (
            <SS.Loader/>
        )
    }

    return (
        <section className="container" style={{width: '100%', height: '100%'}}>
            <div
                {...getRootProps({className: 'dropzone'})}
                style={{
                    width: '100%',
                    height: '100%',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                <input {...getInputProps()} />
                <p>업로드 할 이미지를 올려주세요</p>
            </div>
        </section>
    );
};

export default ReactDropZone;
