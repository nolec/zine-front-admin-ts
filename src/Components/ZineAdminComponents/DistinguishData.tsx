import React from "react";
import SS from "@saraceninc/saracen-style-ts"
import ImageDynamicOptionBar from "./ByOptionDynamic/ImageDynamicOptionBar";
import YoutubeDynamicOptionBar from "./ByOptionDynamic/YoutubeDynamicOptionBar";
import GoodsBarDynamic from "./ByOptionDynamic/GoodsBarDynamic"
import styled from "styled-components";

interface IProps {

}
const Iframe = styled.iframe.attrs({
    frameBorder: '0',
    allow : 'accelerometer',
    autoplay : true,
})`
margin-right:'20px';
`

const DistinguishData : any= ({item, index, noLink, toggleChange, deleteElement, handleInput, noTextArea, noImgTitle, imgTitle, imgColorValue, imgExplanation, divLink,handleYoutubeSize} : any) => {



    if (item.image.includes('https://saracen.azureedge.net/img/template/goods')){
        return(
            <SS.Core.RowF className={item}  style={{'position': 'relative',margin : "17px 0"}} flexDirection='row'>
                <img src={item.image} style={{'maxWidth': '300px','maxHeight':'300px','marginRight':'20px'}} alt={item.alt}/>
                <GoodsBarDynamic
                    index={index}
                    title={item.title}
                    link={item.link}
                    deleteElement={deleteElement}
                    key={index}
                />
            </SS.Core.RowF>
        )
    }else if (item.image.length > 50) {
        return (
            <SS.Core.RowF className={item} style={{'position': 'relative', margin : "17px 0"}} flexDirection='row'>
                <img src={item.image} style={{'maxWidth': '300px','maxHeight':'300px','marginRight':'20px'}} alt={item.alt}/>
                <ImageDynamicOptionBar
                    key={index}
                    index={index}
                    noLink={noLink}
                    toggleChange={toggleChange}
                    deleteElement={deleteElement}
                    handleInput={handleInput}
                    noTextArea={noTextArea}
                    noImgTitle={noImgTitle}
                    imgTitle={imgTitle}
                    imgColorValue={imgColorValue}
                    imgExplanation={imgExplanation}
                    divLink={divLink}
                    item = {item}
                />
            </SS.Core.RowF>
        )
    } else if (item.image.length < 50) {
        return (
            <SS.Core.RowF className={item.title}   style={{'position': 'relative',margin : "17px 0"}} flexDirection='row'>
                <Iframe title='youtube' src={`https://www.youtube.com//embed/${item.image}`} allowFullScreen/>
                <YoutubeDynamicOptionBar
                    item={item}
                    index={index}
                    deleteElement={deleteElement}
                    handleYoutubeSize={handleYoutubeSize}
                    key={index}
                />

            </SS.Core.RowF>
        )
    }


}

export default DistinguishData