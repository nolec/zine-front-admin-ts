import React from "react";
import SS from "@saraceninc/saracen-style-ts"

const GoodsBarDynamic : React.FunctionComponent<any>= ({index,title,link,deleteElement} : any) => {

    return (
        <SS.Core.RowF className={'itemGoods'} alignItems='center' justifyContent='center' flexDirection='column' padding='15px' bgColor='white' style={{width : '500px', height : '130px',borderRadius : '15px'}}>
            <SS.Core.RowF alignItems='center'  flexDirection='column' style={{width : '100%'}}>
                <SS.Core.RowF alignItems='center' flexDirection='row' style={{ margin : '7 0 0 0', width : '100%'}}>
                    <SS.Core.Span>{`상품명 : ${title}`}</SS.Core.Span>
                </SS.Core.RowF>


                <SS.Core.RowF flexDirection='row' style={{width : '100%', margin : '7px 0 0 0'}}>
                    <a href={`https://thesaracen.com/goods/${link}`} target="_blank" rel="noopener noreferrer">
                        <SS.Core.Button padding='11px' className={'blue'} fontSize='15px' style={{'display':'flex','flex':'1 1 0','alignItems':'center','justifyContent':'center',height : '40px'}} >
                            상품 링크
                        </SS.Core.Button>
                    </a>
                    <SS.Core.Button onClick={() => deleteElement(index)} padding='11px' className={'red'} width='100%' fontSize='15px' margin='0 0 0 15px'style={{'flex':'1 1 0', height : '40px'}}>
                        링크 삭제
                    </SS.Core.Button>
                </SS.Core.RowF>
            </SS.Core.RowF>
        </SS.Core.RowF>
    )
}

export default GoodsBarDynamic