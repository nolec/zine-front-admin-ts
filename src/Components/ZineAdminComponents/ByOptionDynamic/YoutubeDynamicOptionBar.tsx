import React from "react";
import SS from "@saraceninc/saracen-style-ts"
import styled from "styled-components";

export const YoutubeWidth = styled.input.attrs(props => ({
    type: 'Number',
    disabled: props.disabled,
    placeholder: props.placeholder
}))`
    width : ${props => props.width ?? '380px'};
    height : 34px;
    border : #e2e2e2 1px black;
    font-size : 13px;
    &:disabled {
        background : #d2d2d2;
        border : 1px;
    }
`


const YoutubeDynamicOptionBar : React.FunctionComponent<any> = ({item,index,deleteElement,handleYoutubeSize} : any) => {

    return (
        <SS.Core.RowF alignItems='center' justifyContent='center' flexDirection='column'  padding='15px'  bgColor='white' style={{width : '500px',height : 'auto',borderRadius : '15px'}}>
            <SS.Core.RowF alignItems='center' justifyContent='center' flexDirection='column'>
                <SS.Core.RowF alignItems='center' justifyContent='center' flexDirection='row' style={{margin : '7 0 0 0'}}>
                    <SS.Core.Label htmlFor={`link${index}`} padding='0 5px'>가로 사이즈</SS.Core.Label>
                    <YoutubeWidth onChange={(e) => handleYoutubeSize(e,index)} value={item.noParsingInputValue ?? ""} width='350px'/>
                </SS.Core.RowF>

                <SS.Core.Button onClick={() => deleteElement(index)} className={'red'} width='100%' fontSize='15px' margin='15px 0 0 0' style={{height : '30px'}}>
                    링크 삭제
                </SS.Core.Button>
            </SS.Core.RowF>
        </SS.Core.RowF>
    )
}

export default YoutubeDynamicOptionBar