import React from "react";
import SS from "@saraceninc/saracen-style-ts"
import styled from 'styled-components'

export const CheckBox = styled.input.attrs(props => ({
    type: 'checkbox',
}))`
`

export const LinkArea = styled.input.attrs(props => ({
    type: 'text',
    disabled: props.disabled,
    placeholder: props.placeholder,
    maxLength : props.maxLength
}))`
    width : ${props => props.width ?? '380px'};
    height : 34px;
    border : #e2e2e2 1px black;
    font-size : 13px;
    &:disabled {
        background : #d2d2d2;
        border : 1px;
    }
`

export const Textarea = styled.textarea.attrs(props => ({
    disabled: props.disabled,
    placeholder: props.placeholder,
    maxLength: props.maxLength
}))`
    width : ${(props : any) => props.width ?? '380px'};
    height : 110px;
    border : #e2e2e2 1px black;
    font-size : 13px;
    &:disabled {
        background : #d2d2d2;
        border : 1px;
    }
`


const ImageDynamicOptionBar : React.FunctionComponent<any> = ({item,index, toggleChange, deleteElement, handleInput} : any) => {

    return (
        <SS.Core.RowF alignItems='center' justifyContent='center' flexDirection='column'  padding='15px'  bgColor='white' style={{width:'500px',height:'auto',borderRadius:'15px'}}>
            <SS.Core.RowF alignItems='center' justifyContent='center' flexDirection='column'>


                <SS.Core.RowF alignItems='center' justifyContent='center' flexDirection='row' style={{margin : '15px 0 0 0'}}>
                    <CheckBox
                        id={index}
                        onChange={() => toggleChange('imgTitle',index)}
                        checked={item.imgTitle.use}
                    />
                    <SS.Core.Label htmlFor={index} padding='0 5px'>제목</SS.Core.Label>
                    <LinkArea
                        width='260px'
                        disabled={!item.imgTitle.use}
                        onChange={(e) => handleInput(e, index, 'imgTitle','value')}
                        value={item.imgTitle.value}
                    />
                    <LinkArea
                        width='120px'
                        disabled={!item.imgTitle.use}
                        onChange={(e) => handleInput(e, index, 'imgTitle','color')}
                        value={item.imgTitle.color}
                        placeholder='컬러값'
                    />
                </SS.Core.RowF>

                <SS.Core.RowF  alignItems='center' justifyContent='center' flexDirection='row'  style={{width:'100%',margin : '7px 0',minHeight : '0'}}>
                    <CheckBox
                        id={`explanation${index}`}
                        onChange={() => toggleChange('imgExplanation', index)}
                        checked={item.imgExplanation.use}
                    />
                    <SS.Core.Label htmlFor={`explanation${index}`} padding='0 5px'>설명</SS.Core.Label>
                    <Textarea
                        disabled={!item.imgExplanation.use}
                        onChange={(e) => handleInput(e, index, 'imgExplanation','value')}
                        value={item.imgExplanation.value}
                    />
                </SS.Core.RowF>


                <SS.Core.RowF alignItems='center' justifyContent='center' flexDirection='row' style={{margin: '7 0 0 0'}}>
                    <CheckBox
                        id={`link${index}`}
                        onChange={() => toggleChange('imgLink', index)}
                        checked={item.imgLink.use}
                    />
                    <SS.Core.Label htmlFor={`link${index}`} padding='0 5px'>링크</SS.Core.Label>
                    <LinkArea
                        disabled={!item.imgLink.use}
                        onChange={(e) => handleInput(e, index, 'imgLink','href')}
                        value={item.imgLink.href}
                    />
                </SS.Core.RowF>

                <SS.Core.Button className={'red'} width='100%' fontSize='15px' margin='15px 0 0 0' onClick={() => deleteElement(index)} style={{height : '30px'}}>
                    이미지 삭제
                </SS.Core.Button>


            </SS.Core.RowF>

        </SS.Core.RowF>
    )
}

export default ImageDynamicOptionBar
