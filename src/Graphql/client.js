import {ApolloClient, InMemoryCache} from "@apollo/client";
import {createUploadLink} from "apollo-upload-client";

const link = createUploadLink({uri : `${process.env.REACT_APP_BACKEND_HOST}`})

const client = new ApolloClient({
    cache: new InMemoryCache({addTypename : false, dataIdFromObject : (object) => object.id}),
    link
})

export default client;
