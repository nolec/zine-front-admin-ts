import {gql} from "@apollo/client"

export const GET_ZINE_TAG_ALL = gql`
    query getZineTagAll{
        getTagsAll{
            id
            tagname
            isMain
        }
    }
`
export const POST_ISMAIN = gql`
    mutation toggleButton(
        $tagId : String!,
        $currentBool : Boolean!,
    )
    {
        updateMainTag(
            tagId : $tagId,
            currentBool : $currentBool
        ){
            id
            tagname
            isMain
        }
    }
`
export const POST_ARTICLE = gql`
    mutation postArticle(
        $newArticle : ZineArticleInput!
        $newContainer : [ZineArticleContentsInput!]!
        $deletedImg : [String!]
    )
    {
        postArticle(
            newArticle : $newArticle,
            newContainer : $newContainer
            deletedImg : $deletedImg
        )
        {
            id
            title
            tags
            createdAt
        }
    }
`

export const UPDATE_ARTICLE = gql`
    mutation update(
        $id : String!,
        $article : ZineArticleInput!
        $updateContainer : [ZineArticleContentsInput!]!
        $deletedImg : [String!]
    )
    {
        updateArticle(
            id : $id ,
            #밑에 article만 써도 되는 이유는 UpdateArticle 안에다가 묶어놨기 때문에 가능합니다.
            article : $article
            updateContainer : $updateContainer
            deletedImg : $deletedImg
        )
        #⬇️해당 부분은 get을 쓸때 필요한 부분인데 이 로직에서는 Get을 따로 빼서 쓰기때문에 쓸필요가없다 id를 쓴이유는 1개 이상은 써야 에러가 안납니다
        {
            id
            title
            tags
            subtitle
            squareThumbnail
            rectangleThumbnail
            isVisible
            photographerName
            editorName
            videoPD
            mainTop
            container{
                alt,
                float,
                image,
                key,
                link,
                width,
                title,
                noParsingInputValue,
                imgExplanation{
                    use,
                    value
                }
                imgTitle{
                    use,
                    value,
                    color
                }
                imgLink{
                    use,
                    href
                }
                price{
                    retailSale,
                    market
                }
            }
        }
    }
`

export const GET_DATA = gql`
    query getData($skip:Float,$limit:Float,$tag:String){
        articles(skip:$skip,limit:$limit,tag:$tag,includeInvisible:false){
            id
            title
            tags
            createdAt
        }
    }
`;

export const GET_POST_DATA = gql`
    query article($id:String!){
        article(id:$id){
            id
            title
            tags
            subtitle
            squareThumbnail
            rectangleThumbnail
            isVisible
            photographerName
            editorName
            videoPD
            mainTop
            container{
                alt,
                float,
                image,
                key,
                link,
                width,
                title,
                noParsingInputValue,
                imgExplanation{
                    use,
                    value
                }
                imgTitle{
                    use,
                    value,
                    color
                }
                imgLink{
                    use,
                    href
                }
                price{
                    retailSale,
                    market
                }
            }
        }
    }

`


export const GET_TOTAL_COUNT = gql`
    query getToTalCount($tag:String){
        totalCount(tag:$tag)
    }
`

export const GET_LIST_DATA = gql`
    query getData($skip:Int,$limit:Int,$tag:String){
        articles(skip:$skip,limit:$limit,tag:$tag,includeInvisible:false){
            id
            title
            tags
            createdAt
        }
        totalCount(tag:$tag)
    }
`;

export const GET_CONTENTS_IMG = gql`
    query getImgURL($file: Upload!) {
        getImgURL(file : $file)
    }
`

export const GET_SQUARE_IMG = gql`
    query getImgURL($file: Upload!) {
        getImgURL(file : $file)
    }
`

export const GET_RECTANGLE_IMG = gql`
    query getImgURL($file: Upload!) {
        getImgURL(file : $file)
    }
`

